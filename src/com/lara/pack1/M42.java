package com.lara.pack1;

public class M42
{

  int test1(boolean flag)
   {
	return 10;
   }
  int test2(boolean flag)
   {
	  if (flag)
	   {
		  return 5;
	   }
	return 10;
    }
  int test3(boolean flag)
  {
	  if (flag)//true return 10
	   {
	   }
	  else//false
	  {
		  return 5;//return
	   }
	return 10;
  }
  int test4(boolean flag)
  {
  if (flag)//true
  {
	  return 10;
  }
 else//false: 5
 {
	  return 5;
  }
  }
//  int test5(boolean flag)
//  {
//  if (flag)//true
//  {
//	  return 10;
//  }
// else//false
// {
//	  return 5;//return
//  }
//  return 20;//this block is unreachable statement 
//}
//int test6(boolean flag)
//{
//if (flag)//true
//{
//	  return 10;
//}
//else//false: no return statement we are getting CTE
// {
//	 
//  }
// }
//int test7(boolean flag)
//{
//if (flag)//true
//{	  
//}
//else//false: 5
//{
//	  return 5;
//}
//}
}