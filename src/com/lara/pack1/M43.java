package com.lara.pack1;

public class M43 
{
   int test1()
   {
	   try
	   {
		   //several statements
	   }
	   catch(ArithmeticException ex)
	   {
		   //some statements
	   }
	   return  10;
   }
   int test2()
   {
	   try
	   {
		   //several statements
		   return 5;
	   }
	   catch(ArithmeticException ex)
	   {
		   //some statements
		  return  10;
	   }
   }
   int test3()
   {
	   try
	   {
		   //several statements
	   }
	   catch(ArithmeticException ex)
	   {
		   //some statements
		  return  10;
	   }
	   return 5;
   }
   int test4()
   {
	   try
	   {
		   //several statements
		   return 10;
	   }
	   catch(ArithmeticException ex)
	   {
		   //some statements
	   }
	   return 5;
   }
//   int tes6()
//   {
//	   try
//	   {
//		   //several statements
//		   return 10;
//	   }
//	   catch(ArithmeticException ex)
//	   {
//		   //some statements
//		  return  20;
//	   }
//	   return 5;//CTE:-without comment
//   }
//   int tes7()
//   {
//	   try
//	   {
//		   //several statements
//		   return 10;
//	   }
//	   catch(ArithmeticException ex)
//	   {
//		   //some statements
//	   }
//   }
//      int tes8()
//   {
//	   try
//	   {
//		   //several statements
//	   }
//	   catch(ArithmeticException ex)
//	   {
//		   //some statements
//		  return  20;
//	   }
//   }
}