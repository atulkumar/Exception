package com.lara.pack1;

public class M24 
{
    public static void main(String[] args) 
    {
		try
		{
			System.out.println(1);//1
			int i=10/0;
			System.out.println(2);//RTE:-arithmetic exception
		}
		catch(ArithmeticException ex)
		{
			System.out.println(3);//3
			int i=10/0;
			System.out.println(4);
		}
		System.out.println(5);
	}
}