package com.lara.pack1;

public class M30 
{
	public static void main(String[] args) 
	  {
		  System.out.println(1);//1
		  int i=0;
		  try
		  {
			  System.out.println(3);//3
			  i=10;
			  System.out.println(4);//4
		  }
		  catch(ArithmeticException ex)
		  {
			  System.out.println(5);
			  i=20;
		  }
		  finally
		  {
			  System.out.println(6);//6
			  i=30;
		  }
		  System.out.println(7);//7
		  System.out.println(i);//30
	  }
}