package com.lara.pack1;

public class M45 
{
     public static void main(String[] args) 
     {
		 System.out.println(1);
		 int i=test();
		 System.out.println(2);
		 System.out.println(i);
     }
     static int test()
     {
    	 try
    	 {
    		 System.out.println(3);
    		 int x=10/0;
    		 System.out.println(4);
    	 }
    	 catch(NullPointerException ex)
    	 {
    		 System.out.println(5);
    	 }
         finally
         {
        	 System.out.println("from finally");
        	 return 20;
         }
     }
}/*output:-
1
3
from finally
2
20
*/