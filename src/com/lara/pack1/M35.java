package com.lara.pack1;
import java.util.Scanner;
public class M35 
{
  public static void main(String[] args) 
  {
	Scanner sc=new Scanner(System.in);
	System.out.println("enter something");
	String s1=sc.next();
	int i=test(s1);
	System.out.println(i);
}
  private static int test(String s1)
  {
	  try
	  {
		  int i=Integer.parseInt(s1);
		  return i;
	  }
	  catch(NumberFormatException ex)
	  {
		  return -10;
	  }
	  finally
	  {
		  return 1000;
	  }
  }
}/*if we are enter a string them control flow to the finally block.*/