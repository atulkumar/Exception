package com.lara.pack1;

public class M27 
{
	 public static void main(String[] args) 
	    {
			try
			{
				System.out.println(1);
				int i=10/0;
				System.out.println(2);
			}
			catch(ArithmeticException ex)
			 {
				 System.out.println(3);
						int i=10/0;
						System.out.println(4);
			 }
			finally
			{
				System.out.println("from finally");
			}
			System.out.println(5);
		}
}/*output:-
1
3
from finally

Exception in thread "main" java.lang.ArithmeticException: / by zero
	at com.lara.pack1.M27.main(M27.java:16)
*/