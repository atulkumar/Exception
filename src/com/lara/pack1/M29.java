package com.lara.pack1;

public class M29
{
  public static void main(String[] args) 
  {
	  System.out.println(1);
	  try
	  {
		  System.out.println(3);
		  int i=10;
		  System.out.println(4);
	  }
	  catch(ArithmeticException ex)
	  {
		  System.out.println(5);
//		  i=20;  //CTE
	  }
	  finally
	  {
		  System.out.println(6);
//		  i=30;  //CTE
	  }
	  System.out.println(7);
//	  System.out.println(i);  //CTE
  }
}//CTE:-local variable cannot be using outside,i allow to only with try block