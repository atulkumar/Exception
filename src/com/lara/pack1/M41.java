package com.lara.pack1;

public class M41
{
	  public static void main(String[] args)
	   {
		  System.out.println(1);//1
		  try
		  {
			  System.out.println(2);//2
			 int i=10/0;
			 System.out.println(2);
		  }
		  catch(ArithmeticException ex)
		  {
			  System.out.println(3);//3
			  System.exit(0);
		  }
		  finally//without finally block executing
		  {
			  System.out.println(4);
		  }
		  System.out.println(5);
	   }
}