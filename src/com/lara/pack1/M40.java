package com.lara.pack1;

public class M40 
{
   public static void main(String[] args)
   {
	  System.out.println(1);
	  try
	  {
		  System.out.println(2);
		 System.exit(0);
		 System.out.println(2);
	  }
	  catch(ArithmeticException ex)
	  {
		  System.out.println(3);
	  }
	  finally//without finally block executing
	  {
		  System.out.println(4);
	  }
	  System.out.println(5);
   }
}/*output:-
1
2         */