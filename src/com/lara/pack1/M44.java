package com.lara.pack1;

public class M44
{
  int test1()
  {
	  try
	  {
		  //several statement
	  }
	  catch(ArithmeticException ex)
	  {
		  //some statement
	  }
	  finally
	  {
		  //some statement
	  }
	  return 5;
  }
  int test2()
  {
	  try
	  {
		  //several statement
		  return 1;
	  }
	  catch(ArithmeticException ex)
	  {
		  //some statement
		  return 2;
	  }
	  finally
	  {
		  //some statement
		  return 3;
	  }
  }
  int test3()
  {
	  try
	  {
		  //several statement
	  }
	  catch(ArithmeticException ex)
	  {
		  //some statement
		  return 2;
	  }
	  finally
	  {
		  //some statement
		  return 3;
	  }
	 // return 5;
  }
  int test4()
  {
	  try
	  {
		  //several statement
	  }
	  catch(ArithmeticException ex)
	  {
		  //some statement
	  }
	  finally
	  {
		  //some statement
		  return 3;
	  }
  }
//	  int test5()
//	  {
//		  try
//		  {
//			  //several statement
//		  }
//		  catch(ArithmeticException ex)
//		  {
//			  //some statement
//		  }
//		  finally
//		  {
//			  //some statement
//			  return 3;
//		  }
//		  return 5;
//	  }
//  int test6()
//  {
//	  try
//	  {
//		  //several statement
//		  return 1;
//	  }
//	  catch(ArithmeticException ex)
//	  {
//		  //some statement
//		  return 2;
//	  }
//	  finally
//	  {
//		  //some statement
//	  }
//	  return 5;
//  }
  }