package com.lara.pack1;

public class M34
{
	public static void main(String[] args) 
	  {
		System.out.println("main begin");
		if(true)
		{
			return;
		}
		try
		{
			System.out.println(1);
			int i=10/0;
		}
		catch(ArithmeticException ex)
		{
			System.out.println(2);
			return;
		}
		finally
		{
			System.out.println(3);
		}
		System.out.println(4);
	  }
}//output:-main begin