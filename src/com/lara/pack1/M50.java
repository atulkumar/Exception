package com.lara.pack1;

public class M50 
{
	public static void main(String[] args)
	{
		System.out.println(1);
		try
		{
			test1();
		}
		catch(ArithmeticException ex)//unchecked exception
		{
			System.out.println(0);
		}
		System.out.println(2);
	}
	static void test1()
	{
		System.out.println(3);
		test2();
		System.out.println(4);
	}
	static void test2()
	{
		System.out.println(5);
		int i = 10 / 0;
		System.out.println(6);
	}
}/*output:-
1
3
5
0
2
*/