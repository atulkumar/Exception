package com.lara.pack4;

public class AgeIsNegativeException extends ArithmeticException
{
	public AgeIsNegativeException()
	{
		
	}
	public AgeIsNegativeException(String msg)
	{
		super(msg);
	}
}
