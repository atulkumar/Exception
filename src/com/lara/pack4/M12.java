package com.lara.pack4;

import java.io.Closeable;
import java.io.IOException;

class A implements Closeable
{
	public void close() throws IOException
	{
		System.out.println("from close");
	}
}
public class M12
{
	public static void main(String[] args)
	{
		System.out.println(1);
		try(A a1=new A();
				A a2=new A();
				A a3=new A())
		{
			System.out.println(2);
		//	a1=new A();
		}
		catch(IOException ex)
		{
			System.out.println(3);
		}
		finally
		{
			System.out.println("from finally");
		}
		System.out.println(4);
	}
}
