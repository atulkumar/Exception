package com.lara.pack3;

import java.sql.SQLException;

public class A 
{
    void test1()
    {
    }
    void test2() throws ClassNotFoundException
    {
    }
    
    void test3() throws SQLException
    {
    }
    void test4() throws NullPointerException
    {
    }
    void test5() throws Exception
    {
    }
}