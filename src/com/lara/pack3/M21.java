package com.lara.pack3;

import java.sql.SQLException;

public class M21
{
	class B extends A
	{
	 void test3() throws SQLException
	    {
	    }
	 void test4() throws NullPointerException
	    {
	    }
	 void test5() throws Exception
	    {
	    }
	}
}/*exception is the all class to the subclass.
*we can test5 method all type exception allows, no matter of checked or unchecked.*/